import { Divide } from "lucide-react";
import { ThemeToggle } from "./ThemeToggle";

const Navbar = () => {
	return (
		<div className="sticky z-50 top-0 inset-x-0 h-20 bg-background border-b-2 border-accent w-full flex items-center justify-end p-4">
			<div className="mr-6">
				<ThemeToggle />
			</div>
		</div>
	);
};

export default Navbar;
