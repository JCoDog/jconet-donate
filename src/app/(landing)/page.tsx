import { ThemeToggle } from "@/components/ThemeToggle";
import { buttonVariants } from "@/components/ui/button";
import {
	Card,
	CardContent,
	CardFooter,
	CardHeader,
} from "@/components/ui/card";
import Link from "next/link";

export default function Home() {
	return (
		<main className="max-h-screen h-full flex flex-col md:overflow-hidden">
			<section className="w-full py-12 md:py-24 lg:py-32 h-full flex items-center justify-center">
				<div className="container px-4 md:px-6">
					<div className="flex flex-col items-center space-y-6 text-center">
						<div className="space-y-2">
							<h1 className="text-3xl font-bold tracking-tighter sm:text-4xl md:text-5xl lg:text-6xl/none">
								Welcome to our{" "}
								<span className="text-sky-500 uppercase">
									new
								</span>{" "}
								donate system
							</h1>
							<p className="mx-auto max-w-[700px] md:text-xl">
								Your support helps us create more content and
								services for content creators and communities.
								Choose the way you want to contribute. You can
								tip (donate) or subscribe.
							</p>
						</div>
						<div className="w-full max-w-md flex flex-col space-y-4 items-center justify-center">
							<Link
								className={buttonVariants({
									variant: "action",
								})}
								href="/tip"
							>
								Tip
							</Link>
							<Link
								className={buttonVariants({
									variant: "action",
								})}
								href="/subscribe"
							>
								Subscribe
							</Link>
						</div>
					</div>
				</div>
			</section>
			<section className="w-full py-12 md:py-24 lg:py-32 h-full flex items-center justify-center bg-gray-100 dark:bg-gray-800">
				<div className="container grid max-w-5xl items-center justify-center gap-4 px-4 text-center md:gap-8 md:px-6 lg:grid-cols-2 lg:text-left xl:max-w-6xl xl:gap-10">
					<div className="flex flex-col justify-center space-y-6">
						<div className="space-y-2">
							<h2 className="text-2xl font-bold tracking-tighter sm:text-3xl md:text-4xl">
								Why Your Support Matters
							</h2>
							<p className="max-w-[500px] md:text-lg/relaxed lg:text-base/relaxed xl:text-lg/relaxed">
								Your donations and subscriptions directly
								support our content creation and helps us
								produce more. As well as helping us produce and
								distribute more services and products such as
								Chloe for content creators and communities.
							</p>
						</div>
					</div>
					<div className="grid w-full grid-cols-1 md:grid-cols-2 items-stretch justify-center gap-4">
						<Card>
							<CardHeader>
								<h3 className="text-xl font-bold">Donate</h3>
							</CardHeader>
							<CardContent>
								<p>
									Your one-time or recurring donation can make
									a big impact on my content creation.
								</p>
							</CardContent>
							<CardFooter className="p-2">
								<Link
									className={buttonVariants({
										variant: "action",
									})}
									href="/tip"
								>
									Tip Now
								</Link>
							</CardFooter>
						</Card>
						<Card>
							<CardHeader>
								<h3 className="text-xl font-bold">Subscribe</h3>
							</CardHeader>
							<CardContent>
								<p>
									Join my community of supporters and get
									updates on my content and special perks.
								</p>
							</CardContent>
							<CardFooter className="p-2">
								<Link
									className={buttonVariants({
										variant: "action",
									})}
									href="/subscribe"
								>
									Subscribe Now
								</Link>
							</CardFooter>
						</Card>
					</div>
				</div>
			</section>
		</main>
	);
}
