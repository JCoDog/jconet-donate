const SubPage = () => {
	return (
		<div className="flex w-full h-full items-center justify-center">
			<h1 className="text-center font-bold text-4xl">
				Subscribe page coming soon...
			</h1>
		</div>
	);
};

export default SubPage;
